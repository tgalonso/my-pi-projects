import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BCM)
GPIO.setup(21,GPIO.IN)
#initialise a previous input variable to 0 (assume button not pressed last)
prev_input = 1
while True:
  #take a reading
  input = GPIO.input(21)
  #if the last reading was low and this one high, print
  if (input):
    print("Butte" )
    print input
  #update previous input
  prev_input = input
  #slight pause to debounce
  time.sleep(0.05)
  