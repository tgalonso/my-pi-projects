#!/usr/bin/python
 
# SIMPLE EXAMPLE :: EXAMPLE SKETCH
# 
# Example sketch for Raspberry Pi that scrolls the lights on and off on the
# LED bar. It uses the LED Strip Python library developed by the LAB at
# Rockwell Group for use with Adafruit's LPD8806 LED strips. 
# 
# Link to LED Strip: http://adafruit.com/products/306

from ledStrip import ledstrip
import time
import random 
import argparse

# Define app description and optional paramerters
parser = argparse.ArgumentParser(description='Example sketch that controls an LED strip via Spacesb. It uses the 	LED Strip Python library for Adafruit\'s LPD8806 LED strips.')

# Define the leds strip length optional parameter
parser.add_argument('-l', '--leds', '--pixels', 
					nargs=1, type=int, default=32,
					help='length of led strip in leds, or pixels')

# read all command line parameters
args = parser.parse_args()


# function that initializes all spacebrew and led strip object when script is run
def main():

	# initialize spi and leds objects
	spidev		= file("/dev/spidev0.0", "wb")  # ref to spi connection to the led bar
	leds 		= ledstrip.LEDStrip(pixels=args.leds, spi=spidev)

	pixel_edge = 0 	# current pixel whose state will be flipped
	turn_on = True  # holds whether pixel will be switched on or off
	y = 0
	print "Turn off"
	while (True):
		for x in range (0,32):
			if y % 20 == 0 :
				leds.setPixelColorRGB(pixel=x, red=32, green=66, blue=64)
			if y % 20 == 1 :
				leds.setPixelColorRGB(pixel=x, red=32, green=65, blue=64)
			if y % 20 == 2 :
				leds.setPixelColorRGB(pixel=x, red=32, green=64, blue=64)
			if y % 20 == 3 :
				leds.setPixelColorRGB(pixel=x, red=32, green=40, blue=64)
			if y % 20 == 4 :
				leds.setPixelColorRGB(pixel=x, red=32, green=32, blue=64)
			if y % 20 == 5 :
				leds.setPixelColorRGB(pixel=x, red=32, green=20, blue=64)
			if y % 20 == 6 :
				leds.setPixelColorRGB(pixel=x, red=32, green=16, blue=64)
			if y % 20 == 7 :
				leds.setPixelColorRGB(pixel=x, red=32, green=10, blue=64)
			if y % 20 == 8 :
				leds.setPixelColorRGB(pixel=x, red=32, green=8, blue=64)
			if y % 20 == 9 :
				leds.setPixelColorRGB(pixel=x, red=32, green=7, blue=64)
			if y % 20 == 10 :
				leds.setPixelColorRGB(pixel=x, red=32, green=6, blue=64)
			if y % 20 == 11 :
				leds.setPixelColorRGB(pixel=x, red=32, green=7, blue=64)
			if y % 20 == 12 :
				leds.setPixelColorRGB(pixel=x, red=32, green=8, blue=64)
			if y % 20 == 13 :
				leds.setPixelColorRGB(pixel=x, red=32, green=10, blue=64)
			if y % 20 == 14 :
				leds.setPixelColorRGB(pixel=x, red=32, green=16, blue=64)
			if y % 20 == 15 :
				leds.setPixelColorRGB(pixel=x, red=32, green=20, blue=64)
			if y % 20 == 16 :
				leds.setPixelColorRGB(pixel=x, red=32, green=32, blue=64)
			if y % 20 == 17 :
				leds.setPixelColorRGB(pixel=x, red=32, green=40, blue=64)
			if y % 20 == 18 :
				leds.setPixelColorRGB(pixel=x, red=32, green=64, blue=64)
			if y % 20 == 19:
				leds.setPixelColorRGB(pixel=x, red=32, green=66, blue=64)
			leds.show()
		y = y + 1
		if y == 20:
			y = 0
		time.sleep(0.5)



if __name__ == "__main__":
	main()